package br.com.itau.Acesso.ClientePorta;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="cliente", configuration = AcessoClienteConfiguracao.class)
public interface AcessoCliente {

    //@GetMapping("/cliente/{idcliente}")
    //Cliente buscarClientePeloId(@PathVariable(name= "idcliente") Long idcliente);

    @GetMapping("/cliente/{id}")
    Cliente buscarClientePeloId(@PathVariable(name = "id") long id);

}

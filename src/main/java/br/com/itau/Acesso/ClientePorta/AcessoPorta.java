package br.com.itau.Acesso.ClientePorta;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name= "porta", configuration = AcessoPortaConfiguracao.class)
public interface AcessoPorta {

    //@GetMapping("porta/{idporta}")
    //Porta buscarPortaPeloId(@PathVariable(name = "idporta") Long idporta);

    @GetMapping("/porta/{id}")
    Porta buscarPortaPeloId(@PathVariable(name = "id") long id);


}

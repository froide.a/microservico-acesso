package br.com.itau.Acesso.ClientePorta;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class AcessoPortaFallback implements AcessoPorta{

    @Override
    public Porta buscarPortaPeloId(long id) {
        throw  new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Serviço Indisponivel - Porta");
    }
}

package br.com.itau.Acesso.ClientePorta;

import br.com.itau.Acesso.Exceptions.ClienteNaoEncontrado;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteCartaoDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();


    @Override
    public Exception decode(String s, Response response){
        if(response.status() == 404){
            return new ClienteNaoEncontrado();
        }
        return errorDecoder.decode(s,response);

    }
}

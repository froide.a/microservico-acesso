package br.com.itau.Acesso.ClientePorta;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ClientePortaFallback implements AcessoCliente {


    @Override
    public Cliente buscarClientePeloId(long id) {
        throw  new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Serviço Indisponivel - Cliente");
    }
}

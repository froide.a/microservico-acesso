package br.com.itau.Acesso.Controller;

import br.com.itau.Acesso.DTO.AcessoEntradaDTO;
import br.com.itau.Acesso.DTO.AcessoSaidaDTO;
import br.com.itau.Acesso.Model.Acesso;
import br.com.itau.Acesso.Service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    AcessoService acessoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AcessoSaidaDTO incluirNovoAcesso(@RequestBody @Valid AcessoEntradaDTO acessoEntradaDTO) {

        return acessoService.incluirNovoAcessoAPorta(acessoEntradaDTO);

    }

    @GetMapping(value = "/{idcliente}/{idporta}")
    @ResponseStatus(HttpStatus.OK)
    public AcessoSaidaDTO buscarAcessoPorId(@PathVariable(value = "idcliente") Long idcliente, @PathVariable(value = "idporta") Long idporta){

        return  acessoService.buscarAcessoPorId(idcliente, idporta);

    }

    @PutMapping(value = "/DELETE/{idcliente}/{idporta}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void excluirAcesso(@PathVariable(value = "idporta") Long idporta, @PathVariable(value = "idcliente") Long idcliente){

         acessoService.excluirAcesso(idporta, idcliente);

    }


}

package br.com.itau.Acesso.DTO;

import br.com.itau.Acesso.Model.Acesso;

public class AcessoEntradaDTO {

    private long idporta;
    private long idcliente;

    public AcessoEntradaDTO() {
    }

    public long getIdporta() {
        return idporta;
    }

    public void setIdporta(long idporta) {
        this.idporta = idporta;
    }

    public long getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(long idcliente) {
        this.idcliente = idcliente;
    }
}

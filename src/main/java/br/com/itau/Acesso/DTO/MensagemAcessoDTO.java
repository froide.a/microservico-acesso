package br.com.itau.Acesso.DTO;

public class MensagemAcessoDTO {

    private long id;
    private long idporta;
    private long idcliente;
    private Boolean acessoautorizado = false ;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdporta() {
        return idporta;
    }

    public void setIdporta(long idporta) {
        this.idporta = idporta;
    }

    public long getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(long idcliente) {
        this.idcliente = idcliente;
    }

    public Boolean getAcessoautorizado() {
        return acessoautorizado;
    }

    public void setAcessoautorizado(Boolean acessoautorizado) {
        this.acessoautorizado = acessoautorizado;
    }
}

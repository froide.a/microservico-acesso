package br.com.itau.Acesso.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT,reason = "Acesso ja existe!" )
public class AcessoJaCadastrado extends RuntimeException {
}

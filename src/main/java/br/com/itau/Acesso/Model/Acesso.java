package br.com.itau.Acesso.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Acesso {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;


    @NotNull(message = "Informe o id da porta")
    private long  idporta;

    @NotNull(message = "Informe o id do cliente")
    private long idcliente;

    private Boolean acessoautorizado = false ;

    public Boolean getAcessoautorizado() {
        return acessoautorizado;
    }

    public void setAcessoautorizado(Boolean acessoautorizado) {
        this.acessoautorizado = acessoautorizado;
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdporta() {
        return idporta;
    }

    public void setIdporta(long idporta) {
        this.idporta = idporta;
    }

    public long getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(long idcliente) {
        this.idcliente = idcliente;
    }
}

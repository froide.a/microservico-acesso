package br.com.itau.Acesso.Repository;

import br.com.itau.Acesso.Model.Acesso;
import org.aspectj.weaver.patterns.PerThisOrTargetPointcutVisitor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.Optional;

public interface AcessoRepository extends CrudRepository<Acesso, Long> {

    Optional<Acesso> findByIdclienteAndIdporta(long idporta, long idcliente);

    @Transactional
    void removeAllByIdclienteAndIdporta(long idporta, long idcliente);
}

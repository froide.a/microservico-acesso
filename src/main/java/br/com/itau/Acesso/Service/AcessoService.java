package br.com.itau.Acesso.Service;

import br.com.itau.Acesso.ClientePorta.AcessoCliente;
import br.com.itau.Acesso.ClientePorta.AcessoPorta;
import br.com.itau.Acesso.ClientePorta.Cliente;
import br.com.itau.Acesso.ClientePorta.Porta;
import br.com.itau.Acesso.DTO.AcessoEntradaDTO;
import br.com.itau.Acesso.DTO.AcessoSaidaDTO;
import br.com.itau.Acesso.Exceptions.AcessoJaCadastrado;
import br.com.itau.Acesso.Exceptions.AcessoNaoAutorizado;
import br.com.itau.Acesso.Model.Acesso;
import br.com.itau.Acesso.Repository.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AcessoService {


    @Autowired
    AcessoRepository acessoRepository;

    @Autowired
    AcessoCliente acessoCliente;

    @Autowired
    AcessoPorta acessoPorta;

    @Autowired
    private KafkaTemplate<Object, Acesso> producer;

    public AcessoSaidaDTO incluirNovoAcessoAPorta(AcessoEntradaDTO acessoEntradaDTO) {

        Optional<Acesso> optionalAcesso = acessoRepository.findByIdclienteAndIdporta( acessoEntradaDTO.getIdporta(), acessoEntradaDTO.getIdcliente());

        if(!optionalAcesso.isPresent()) {

            Porta porta = acessoPorta.buscarPortaPeloId(acessoEntradaDTO.getIdporta());
            Cliente cliente = acessoCliente.buscarClientePeloId(acessoEntradaDTO.getIdcliente());

            Acesso acesso = new Acesso();

            acesso.setIdporta(acessoEntradaDTO.getIdporta());
            acesso.setIdcliente(acessoEntradaDTO.getIdcliente());

            enviarMensagemKafka(acesso, true);

            acessoRepository.save(acesso);

            return new AcessoSaidaDTO(acesso);
        }else {
            throw new AcessoJaCadastrado();
        }

    }

    public AcessoSaidaDTO buscarAcessoPorId(long idporta, long idcliente) {
        Optional<Acesso> optionalAcesso = acessoRepository.findByIdclienteAndIdporta(idporta, idcliente);

        if (optionalAcesso.isPresent()) {

            Acesso acesso = optionalAcesso.get();

            enviarMensagemKafka(acesso, true);

            return new AcessoSaidaDTO(acesso);

        } else {
            Acesso acesso = new Acesso();
            acesso.setIdporta(idporta);
            acesso.setIdcliente(idcliente);

            enviarMensagemKafka(acesso, false);

            throw new AcessoNaoAutorizado();

        }

    }

    public void excluirAcesso(long idporta, long idcliente) {


         acessoRepository.removeAllByIdclienteAndIdporta(idporta, idcliente);

    }

    public void enviarMensagemKafka(Acesso acesso, boolean autorizado) {
        acesso.setAcessoautorizado(autorizado);

        producer.send("spec4-alex-froide-1", acesso);
    }
}
